var express = require('express');
var router = express.Router();

var dataAPI = require('../services/dataAPI');
var monitoring = require('../services/monitoring');
var history = require('../services/history');
var marketPrediction = require('../services/marketPrediction');

/* GET coin operation. */
router.get('/start/:coin', function(req, res, next) {

    const coin = req.params.coin;

    monitoring.startScheduler(coin, function(){
        // Fetching data for coin
        dataAPI.getInfo(coin, 'ticker').then((response) => {
            let jsonResponse = JSON.parse(response.body)
            // Writing to the history
            history.write(coin, jsonResponse);
        });
    });

    res.json({ active: monitoring.isActive(coin) });
});

router.get('/history/:coin', function(req, res, next) {
    const coin = req.params.coin;
    res.send(history.getSection(coin));
});
    
router.get('/stop/:coin', function(req, res, next) {  
    const coin = req.params.coin;
    monitoring.stopShcheduler(coin)

    res.json({ active: monitoring.isActive(coin) });
});



module.exports = router;
