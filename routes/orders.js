var express = require('express');
var router = express.Router();

var orders = require('../services/orders');

router.get('/listMyTransactions', function(req, res, next){
    res.send(orders.listMyTransactions());
})

router.get('/createBuyTransaction/:targetValue', function(req, res, next){
    const targetValue = req.params.targetValue;
    res.send(orders.createBuyTransaction('btc', targetValue, 0.03, 0.05));      
})

module.exports = router;
