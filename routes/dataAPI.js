var express = require('express');
var router = express.Router();

var dataAPI = require('../services/dataAPI')

/* GET coin operation. */
router.get('/:coin/:operation', function(req, res, next) {
  const coin = req.params.coin;
  const operation = req.params.operation;

  dataAPI.getInfo(coin, operation).then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(error);
    res.send(500);
  });
});

module.exports = router;
