# Bot

## Data API endpoints:

The data API does not require authentication. 

/data/COIN/OPERATION

Available options for COIN:
* BTC: Bitcoin
* BCH: Bitcoin Cash
* LTE: Lite Coin

Available options for OPERATION:
* orderbook: shows the orderbook for a specific coin
* ticker: shows basic info for a specific coin
* trades: shows trade info for a specific coin

## Negotiation API endpoints:

To be implemented.

## Monitoring Market 

To start/stop monitoring a coin use the following endpoint:

/monitoring/INSTRUCTION/COIN

Available options for INSTRUCTION: 
* start: Start the monitoring for a specific coin
* stop: Stop the monitoring for a specific coin

Available options for COIN:
* BTC: Bitcoin
* BCH: Bitcoin Cash
* LTE: Lite Coin

The algorithm for market prediction is not implemented yet.