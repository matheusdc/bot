var stats = require('stats-lite');
var SMA = require('technicalindicators').SMA;
var BB = require('technicalindicators').BB;
var RSI = require('technicalindicators').RSI;

/**
 * 
 * 
 * @class MarketPrediction
 */
class MarketPrediction {

    /**
     *
     * @param {any} level 
     * @memberof MarketPrediction
     */
    constructor(level){
        
        this.sma = [];
        this.bb = [];
        this.rsi = [];

        // The confidence level states how many evidences it should have to decide to buy
        this.confidenceThreshold = level;
    }

    /**
     *
     * 
     * @param {any} values 
     * @memberof MarketPrediction
     */
    calculateSMA(values) {

    }

    /**
     * 
     * 
     * @param {any} values Array of closing values with minimum size of period
     * @param {any} period look back period - the default value is 14
     * @param {any} stdDev standard deviation value - it is common to use two values (1 and 2) in order to plot multiple BB
     * @memberof MarketPrediction
     */
    calculateBB(values, period = 14, stdDev = 1){
        var inputBB = {
            values,
            period,
            stdDev
        };

        return BB.calculate(inputBB);
    }

    /**
     * 
     * 
     * @param {any} values Array of closing values with minimum size of period.
     * @param {any} period look back period - the default value is 14. Can be lowered to increase sensitivity or raised to decrease it sensitivity
     * @returns RSI of each day from 0 to 100 
     * 
     * @memberof MarketPrediction
     */
    calculateRSI(values, period = 14){        
        var inputRSI = {
            values,
            period
        };
        
        return RSI.calculate(inputRSI);
    }

    /**
     * 
     * 
     * @memberof MarketPrediction
     */
    guess() {
        // This variable has the number of evidences that ensure a good buying,
        // each buying strategy that validates increases this counter
        // If the evidences are greater than the confidence threshold than the bot decides to buy or not
        let buyEvidences = 0;

        // STRATEGIES
        
        // 1) RSI
        // if RSI is smaller than 30 and rising its a potencial bullish signal

        // 2) Bollinger Bands
        // When the lowest price is at the lower BB and the the next is increasing, its a potential turn around signal

        // More strategies soon...
    }
}

module.exports = new MarketPrediction();