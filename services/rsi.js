/**
 * 
 * Developed by Welles Wilder, the Relative Strenght Index (RSI) is an indicator know as momentum oscillator.
 * 
 * The Relative Strength Index is a useful for determining when prices are overbought or oversold, 
 * but it should not be the only indicator for traders when making decisions
 * 
 * The indicator measures the velocity and magnitude of prices moves and oscillates between 0 and 100. 
 * When the value is higher than a set threshold, let say 70, we can consider overbought and lower than 30 oversold.
 * 
 * The indicator is calculated using the average gains and losses of an asset over a specified time period. 
 * The default look-back setting for the indicator suggested by Wilder is 14 periods. 
 * 
 * RSI = 100 - 100 / (1 + RS)
 *
 * Where RS = Average gain of up periods during the specified time frame / Average loss of down periods during the specified time frame
 * 
 * Please, read https://www.tororadar.com.br/investimento/analise-tecnica/indice-de-forca-relativa-ifr to get a simple understading of RSI
 * 
 * @class RSI
 * 
 */
class RSI {



}