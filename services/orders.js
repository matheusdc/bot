const https = require('https');
const crypto = require('crypto');
const unirest = require('unirest');
const qs = require('query-string');
var schedule = require('node-schedule');
var dataAPI = require('./dataAPI');

const ENDPOINT_API = 'https://www.mercadobitcoin.com.br/api/';
const ENDPOINT_TRADE_PATH = "/tapi/v3/";
const ENDPOINT_TRADE_API = 'https://www.mercadobitcoin.net' + ENDPOINT_TRADE_PATH;

const WAITING_TO_BUY = 0;
const WAITING_TO_SELL = 1;

/**
 * This class uses Mercado Bitcon TAPI and a scheduling system to buy and sell coins automatically
 * 
 * Do not forget to create a file in your project root directory called .env with the following constants
 * CRAWLER_INTERVAL=60 
   KEY= <your-ket>
   SECRET= <your-secret>
   PIN= <your-pin>
 * 
 * @class Orders
 */
class Orders {
  constructor () {

    /**
     * Needed variables to perform any actions using Mercado Bitcoin TAPI
     */
    this.config = {
        KEY: process.env.KEY,
        SECRET: process.env.SECRET,
        PIN: process.env.PIN,
    }

    /**
     * Schedule rule. Every 5 seconds
     */
    this.rule = new schedule.RecurrenceRule();
    this.rule.second = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];

    /**
     *  Keeps all the transactinos scheduled
     */
    this.transactionsStatus = {};
  }
  
  /**
   * 
   * 
   * @param {any} success 
   * @param {any} error 
   * @memberof Orders
   */
  getAccountInfo(success, error){
    this.call('get_account_info', {}, success, error);
  }

  /**
   * List all the pending transactions
   * 
   * @param {any} parameters 
   * @param {any} success 
   * @param {any} error 
   * @returns {array} 
   * @memberof Orders
   */
  listMyTransactions(parameters, success, error){
    const obj ={
        transactionsStatus: this.transactionsStatus
    }
    return obj;
  }

  /**
   * 
   * This functions create a schedule to buy and sell coins
   * Set the minimum value you want to buy a coin and the minGain you want to achieve
   * This will create job that runs every 5 seconds to check whether the conditions to buy are correct 
   * 
   * Once the condition to buy is met a request will be made to buy the coin using the current price market
   * When the % of profit is met a request will be made to sell the coin using the current price market
   * 
   * @param {any} coin Coin we are going to buy (BTC, BCH or LTC)
   * @param {any} targetValuetoBuy Target value to buy a coin
   * @param {any} minGain This value should be at least 0.03 in order to guarantee 1.5% of profit
   * @param {any} maxGain not used yet
   * @returns 
   * @memberof Orders
   */
  createBuyTransaction(coin, targetValuetoBuy, minGain, maxGain){
    
    var transactionId =   Math.round(new Date().getTime() / 1000);

    // Current transactions data
    this.transactionsStatus[transactionId] = {
      job: null,
      status: WAITING_TO_BUY,
      targetValuetoBuy: targetValuetoBuy,
      minGain: minGain,
      maxGain: maxGain,
      coin: coin
    };

    var that = this;
    var current_transaction = schedule.scheduleJob(this.rule, function(data){
      const coin = data.coin;

      // Fetching data for coin
      dataAPI.getInfo(coin, 'ticker').then((response) => {
          const lastEntry = JSON.parse(response.body).ticker;
        
          // We we are still waiting to buy a coin (the target price was not met yet)
          if(data.status === WAITING_TO_BUY){
    
            // If the current value is smaller than the value we are willing to buy then create the transaction
            console.log(`${lastEntry.buy} < ${data.targetValuetoBuy}`)
            if(lastEntry.buy < data.targetValuetoBuy ){
              // Here we should create a buy order using the current ask market price
              console.log('YAY! Time to buy');
              
              // TODO Important! Do not forget to check if the purchase was made before settings this variable to WAITING_TO_SELL
              data.status = WAITING_TO_SELL;

              // Here we have to actually put the price we bought
              data.buyValue = lastEntry.buy;
            }
            else {
              console.log('Waiting a better opportunity');
            }
          }
          else if(data.status === WAITING_TO_SELL){
    
            console.log(`${lastEntry.sell} >= (Target Value: ${data.buyValue * (1 + data.minGain)}) (Buy value: ${data.buyValue})`);
            if(lastEntry.sell >= data.value * (1 + data.minGain) ){
              // Here we should create a sell order using the current bid market price
              console.log('YAY! Time to sell!');
            }
            else {
              console.log('NOPE! Just wait a little bit to sell');
            }
          }
      });
    }.bind(null, this.transactionsStatus[transactionId]));

    this.transactionsStatus[transactionId].job = current_transaction;    
    return current_transaction;
  }

  /**
   * Called by CreateBuyTransaction to place a buy order using the current market value
   * 
   * @param {any} parameters 
   * @param {any} success 
   * @param {any} error 
   * @memberof Orders
   */
  _placeBuyOrder(parameters, success, error){

  }

  /**
   * 
   * 
   * @param {any} parameters 
   * @param {any} success 
   * @param {any} error 
   * @memberof Orders
   */
  _placeSellOrder(parameters, success, error){

  }

  /**
   * 
   * 
   * @param {any} parameters 
   * @param {any} success 
   * @param {any} error 
   * @memberof Orders
   */
  _cancelOrder(parameters, success, error){

  }

  /**
   * Performs a call to Mercado Bitcoin TAPI
   * It receives the method name as parameter - the methods are listed at https://www.mercadobitcoin.com.br/trade-api/
   * 
   * @param {any} method 
   * @param {any} parameters 
   * @param {any} success 
   * @param {any} error 
   * @memberof Orders
   */
  call(method, parameters, success, error){
    var now = Math.round(new Date().getTime() / 1000);

    // Build the URL parameters - tapi_method=method&tapi_nonce=now
    var queryString = qs.stringify({
      'tapi_method': method,
      'tapi_nonce': now
    });

    // We could receive more parameters depending on each function 
    if(parameters) {
      queryString += '&' + qs.stringify(parameters);
    }

    // Uses nodejs crypto library to cypher the secret and keys
    var signature = crypto.createHmac('sha512', this.config.SECRET)
                          .update(ENDPOINT_TRADE_PATH + '?' + queryString)
                          .digest('hex');


    unirest.post(ENDPOINT_TRADE_API)
           .headers({'TAPI-ID': this.config.KEY})
           .headers({'TAPI-MAC': signature})
           .send(queryString)
           .end(function (response) {
               if(response.body){
                  if (response.body.status_code === 100 && success)
                      success(response.body.response_data)
                  else if(error)
                      error(response.body.error_message)
                  else
                      console.log(response.body)
               }
               else console.log(response)
           })
  }
}

module.exports = new Orders();