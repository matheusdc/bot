/**
 * Multi-key monitoring system
 * 
 * This class allows user to schedule tasks on specific keys.
 * For simplicity the scheduler rule is the same for all keys.
 * 
 * The available methods are:
 * 
 * * startScheduler(key, method) - Schedule a method and register it on key
 * * stopScheduler(key) - Stop a scheduler on key
 * * getSchedulerRule() - Returns the scheduler rule
 * * isActive(key) - check if a scheduler is active for key
 * 
 */

var schedule = require('node-schedule');

class Monitoring {
    constructor(){
        // For simplicity the scheduler use the same rule for all the keys
        this.rule = new schedule.RecurrenceRule();
        this.rule.second = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
    
        // Schedule object
        this.task = {};
    }

    isActive(key) {
        return typeof this.task[key] !== 'undefined' && this.task[key] !== null;
    }

    startScheduler(key, method) {
        if(this.isActive(key)) {
            this.stopShcheduler(key);
        }
        return this.task[key] = schedule.scheduleJob(this.rule, method);
        
    }

    stopShcheduler(key) {
        if(this.isActive(key)){
            this.task[key].cancel()
            this.task[key] = null;
            return true;    
        }
        return false;
    }

    getSchedulerRule() {
        return this.rule;
    }
}

module.exports = new Monitoring();