/**
 * History 
 * 
 * The history contains the info ticker info from each coin.
 * 
 * Methods:
 * * getSection(key) - Get a section that has been writen
 * * write(key, sentence) - Write sentence to the section
 * * clearSection(key) - delete section named
 * 
 */
class History {
    constructor(){
        this.numberOfPages = 10000;
        this.section = {};
    }

    getSection(key) {
        return this.section[key];
    }

    write(key, sentence) {
        // If the section does not exists, create a new one
        if(typeof this.section[key] === 'undefined') this.section[key] = [];
        // If section is full remove the first element
        if(this.section[key].length > this.numberOfPages) {
            this.section[keys].splice(0, 1);
        }
        // Write the sentence to the section
        this.section[key].push(sentence);
    }

    clearSection() {
        delete this.section[key];
    }
}

module.exports = new History();