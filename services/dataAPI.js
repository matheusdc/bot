const request = require('request');

class dataAPI {
    constructor(){ 
        this.baseURL = "https://www.mercadobitcoin.net/api/";
    }

    getInfo(coin, operation) {
        // Creating a promise to handle async queries
        return new Promise((resolve, reject) => {
            // Assembling the URL
            const url = `${this.baseURL}/${coin}/${operation}`;

            // Checking if coin or operator is defined
            if(typeof coin === 'undefined' || operation === 'undefined')
                reject(Error('Keyword undefined'));

            // Final query Url preview for debugging purpouses
            console.log(`Fetchin ${coin} ${operation} info from ${url}`);

            // Creating the header for the crawler
            var headers = {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
                'Accept': '*/*'
            }

            // Creating the search request
            request({
                headers,
                uri: url
            }, (error, response, html) => {
                // Checking if everything went okay
                if(!error)
                    resolve(response);
                else
                    reject(error);
            });
        });
    }
}

module.exports = new dataAPI();